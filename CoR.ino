#include "Charliplexing.h"      //initializes the LoL Sheild library
#include "Sprites.h"

const uint8_t DELAY_MS = 25;
const uint8_t SPRITE_INTERVAL = 5;
const uint8_t SCROLL_WAIT_FRAMES = 30;

uint8_t blinkIncr = 0;
uint8_t scrollIncr = 0;
uint32_t scrollSteps = 0;
uint8_t scrollWait = SCROLL_WAIT_FRAMES;
uint8_t animIncr = 0;
uint8_t animFrame = 0;

// This function is called once, when the sketch starts.
void setup() {
  LedSign::Init(GRAYSCALE); // Initializes a grayscale frame buffer.
}

// This function is called every frame.
void loop() {
  uint8_t blink0 = (blinkIncr>>1) & 0b111;
  uint8_t blink1 = ((blinkIncr & 0b111) << 1) | 0b1;  // brighter, faster
  
  uint8_t animOffset = BMP_HEIGHT * ((animFrame % 2) ? 0 : ((animFrame==0)?1:2));
  uint8_t sprIdx = ((scrollSteps+BMP_HEIGHT) / (BMP_HEIGHT*SPRITE_INTERVAL)) % SPRITE_VARIATIONS;
    
  for( uint8_t j=0; j<14; j++ ) {
    uint32_t arrowIdx = (scrollSteps+j)/BMP_HEIGHT;
    
    boolean isSprite = ((arrowIdx % SPRITE_INTERVAL) == 0);
    
    for( uint8_t i=0; i<9; i++ ) {
      prog_uchar color;
      
      if( isSprite ) {
        uint8_t line = ((j+scrollSteps)%BMP_HEIGHT) + animOffset;
        if( sprIdx==0 ) {
          color = pgm_read_byte_near( &gruntBitmap[line][i] );
        } else if( sprIdx==1 ) {
          color = pgm_read_byte_near( &saviorBitmap[line][i] );
        } else if( sprIdx==2 ) {
          color = pgm_read_byte_near( &daddyBitmap[line][i] );
        }
        
      } else {
        color = pgm_read_byte_near( &arrowBitmap[(j+scrollSteps)%BMP_HEIGHT][i] );
      }
      
      if( color==BX ) {
        LedSign::Set( j, 8-i, blink0 );
      } else if( color==BO ) {
        LedSign::Set( j, 8-i, blink1 );
      } else {
        LedSign::Set( j, 8-i, color );
      }
    }
  }

  
  // Increment everything
  blinkIncr++;
  
  if( scrollWait > 0 ) {
    scrollWait--;
  } else {
    scrollIncr++;
    if( scrollIncr >= 2 ) {
      scrollIncr = 0;
      scrollSteps++;
      
      // When a Sprite arrives, pause for a few frames
      uint8_t spriteOffset = scrollSteps % (SPRITE_INTERVAL * BMP_HEIGHT);
      if( spriteOffset==0 ) {
        scrollWait = SCROLL_WAIT_FRAMES;
      }
    }
  }
  
  animIncr++;
  if( animIncr >= 4 ) {
    animIncr = 0;
    animFrame = (animFrame+1) % 4;
  }
  
  delay(DELAY_MS);
}

